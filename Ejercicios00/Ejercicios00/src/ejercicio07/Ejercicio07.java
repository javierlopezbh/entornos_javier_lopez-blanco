package ejercicio07;

public class Ejercicio07 {
	public static void main(String args[]) {
		int numero1 = 45;
		System.out.println("incremento");
		System.out.println("numero1 " + numero1);
		int numero2 = numero1 + 10;
		System.out.println("numero2 " + numero2);
		System.out.println("decremento");
		int numero3 = numero1 - 10;
		System.out.println("numero3 " + numero3);
		System.out.println("operador +=");
		int numero4 = 10;
		System.out.println("numero4 " + numero4);
		numero4 += 3;
		System.out.println("operador -=");
		System.out.println("numero4 " + numero4);
		int numero5 = 17;
		System.out.println("numero5 " + numero5);
		numero5 -= 2;
		System.out.println("numero5 " + numero5);
		System.out.println("operador *=");
		int numero6 = 2;
		System.out.println("numero6 " + numero6);
		numero6 *= 3;
		System.out.println("numero6 " + numero6);
		System.out.println("operador /=");
		int numero7 = 20;
		System.out.println("numero7 " + numero7);
		numero7 /= 2;
		System.out.println("numero7 " + numero7);
		System.out.println("operador ++");
		int numero8 = 1;
		System.out.println("numero8 " + numero8);
		numero8++;
		System.out.println("numero8 " + numero8);
		System.out.println("operador --");
		int numero9 = 5;
		System.out.println("numero9 " + numero9);
		numero9--;
		System.out.println("numero9 " + numero9);

	}
}
